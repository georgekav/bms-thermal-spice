__author__ = 'bansal'
from remote_data_fetcher import *
import os
import subprocess
from time import time


def building_model(room_ids_simulate_list, sim_start_point, sim_end_point):
    building_data= get_static_data(1)
    zone_list=building_data["Zonelist"]
    bd_element_list= building_data["ZoneBorderlist"]
    heater_list=building_data["ZoneHeaterlist"]
    print heater_list
    node=1 # counter to keep track of nodes
    cap_count=1 # counter to keep track of capacitances
    res_count=1 # counter to keep track of resitances
    volt_count=1# counter to keep track of voltage sources
    current_count=1 # counter to keep track of current source
    lines=["This is a work of EPFL"] # first line in the netlist is not read
    d_a=1.2041 # density of air
    cp_a=1012 # specific heat capacity of air
    zone_node_dict={} # create a dictionary to store different zones and their associated node
    mode=2 # this is a dummy variable which specifies the type of simulation we want to run. If it is not equal to 1 means that
    # we dont have the surrounding room temperatures but only the outside temperatures. So the entire building has to be simulated
    for zone in zone_list:
        if mode==1:
            if zone["roomID"] in room_ids_simulate_list:
                zone_id_sim = zone["zoneID"]
                [lines, zone_node_dict, node, cap_count]=write_zone_capacitors(zone, lines, zone_node_dict, node, cap_count, d_a,cp_a)
            else:
                [lines, zone_node_dict, node, volt_count]=write_voltage_sources(zone, lines, zone_node_dict, node, volt_count, sim_start_point, sim_end_point)

        else:
              if zone["roomID"] in room_ids_simulate_list or zone["roomID"]!= 7:
                zone_id_sim = zone["zoneID"]
                [lines, zone_node_dict, node, cap_count]=write_zone_capacitors(zone, lines, zone_node_dict, node, cap_count, d_a,cp_a)
              else:
                [lines, zone_node_dict, node, volt_count]=write_voltage_sources(zone, lines, zone_node_dict, node, volt_count, sim_start_point, sim_end_point)

    # write resistances and capacitances for building elements like walls and windows
    [lines, zone_node_dict, node, cap_count, res_count]= write_building_element(bd_element_list, lines,zone_node_dict, node, cap_count, res_count)
    [lines, zone_node_dict, node, current_count]= write_heater_element(heater_list, lines,zone_node_dict, node, current_count)
    lines.append(".tran 0 86100 0 120")
    lines.append(".backanno")
    lines.append(".end")
    return [lines, res_count, volt_count, cap_count, zone_node_dict, zone_id_sim]

def write_zone_capacitors(zone, lines, zone_node_dict, node, cap_count, d_a,cp_a):
    zone_node_dict[zone["zoneID"]]="n"+str(node)
    cap_value=d_a*cp_a*zone["volume"]
    current_line=["C"+str(cap_count)+" "+str(0)+" "+"n"+str(node)+" "+str(cap_value)]
    cap_count+=1
    node+=1
    lines=lines+current_line
    return[lines, zone_node_dict,node, cap_count]

def write_voltage_sources(zone, lines, zone_node_dict, node, volt_count, sim_start_point, sim_end_point):
    zone_node_dict[zone["zoneID"]]="n"+str(node)
    volt_value=str(get_dynamic_data(roomid=zone["roomID"], f_time=sim_start_point, t_time=sim_end_point, res=120))
    current_line = ["V"+str(volt_count)+" "+"n"+str(node)+" "+str(0)+" "+"PWL"+str(volt_value)]
    volt_count+=1
    node+=1
    lines=lines+current_line
    return [lines, zone_node_dict, node, volt_count]

def write_heater_element(heater_list, lines,zone_node_dict, node, current_count):
    for heater in heater_list:
        heater_node = zone_node_dict[heater["zoneID"]]
        heater_power = heater["HeatValue"]
        lines=lines+["I"+str(current_count)+ " " + "0" +" "+heater_node+" "+"PULSE(0 "+str(heater_power)+" 10 0 0 86100 86100 1)"]
        current_count+=1
    return(lines, zone_node_dict,node, current_count)


def write_building_element(bd_element_list, lines,zone_node_dict, node, cap_count, res_count):
    for bd_element in bd_element_list:
            side_a = zone_node_dict[bd_element["l_zoneID"]]
            side_b = zone_node_dict[bd_element["r_zoneID"]]
            area = bd_element["surface"]
            conv_coeff_a = bd_element["zb_attr"]["conjA"]
            conv_coeff_b = bd_element["zb_attr"]["conjB"]
            cp=[]
            d=[]
            rho=[]
            t=[]
            if bd_element["window_attr"]!=None: # check if the wall has a window
                win_area = bd_element["window_surface"]
                u_value = bd_element["window_attr"]["uvalue"]
                area=area-win_area # subtracting the area of window from wall area
                if area >=0:
                    res_win=1/(win_area*u_value)
                    lines=lines+["R"+str(res_count)+" "+str(side_a)+" "+str(side_b)+" "+str(res_win)]
                    node+=1
                    res_count+=1
                else:
                    raise ValueError("window area cannot be larger than wall area")
            i=0;
            for material in  bd_element["zb_attr"]["matlist"]:
                cp = cp+ [bd_element["zb_attr"]["matlist"][i]["matterial"]["cp"]]
                d = d + [bd_element["zb_attr"]["matlist"][i]["matterial"]["D"]]
                rho = rho+ [bd_element["zb_attr"]["matlist"][i]["matterial"]["p"]]
                t=t+[bd_element["zb_attr"]["matlist"][i]["thickness"]]
                i=i+1
            [res_value,cap_value,res_conv_a, res_conv_b]=compute_resistance(cp,d,rho,t, area, conv_coeff_a, conv_coeff_b)
            # writing the wall resistance and capacitances
            [current_line, cap_count, res_count, node]= compose_line(res_count,cap_count, res_value,cap_value, node, side_a, side_b,res_conv_a,res_conv_b)
            lines=lines+current_line
    return [lines, zone_node_dict, node, cap_count, res_count]


#  compute equivalent resistance from the wall data
def compute_resistance(cp,d,rho,t, area, conv_a, conv_b):
    total_t=sum(t)
    davg = sum ([d[i]*t[i] for i in range(len(d))])/total_t
    cpavg = sum ([cp[i]*t[i] for i in range(len(d))])/total_t
    cpavg = sum ([cp[i]*t[i] for i in range(len(d))])/total_t
    rhoavg = sum ([rho[i]*t[i] for i in range(len(d))])/total_t
    res = (rhoavg*total_t)/(2*area)
    cap = davg*total_t*area*cpavg
    res_conv_a=1/(area*conv_a)
    res_conv_b=1/(area*conv_b)
    return [res,cap, res_conv_a,res_conv_b]

# compose the line for each wall. For now it only models a 1C-2R wall
def compose_line(res_count, cap_count, res_value, cap_value, node, side_a, side_b, res_conv_a, res_conv_b):
    line=[]
    line=line+["R"+str(res_count)+" "+str(side_a)+" "+"n"+str(node+1)+" "+str(res_conv_a)]
    res_count+=1
    node+=1
    line=line+["R"+str(res_count)+" "+"n"+str(node)+" "+"n"+str(node+1)+" "+str(res_value)]
    res_count+=1
    node+=1
    line=line+["C"+str(cap_count)+" "+str(0)+" "+"n"+str(node)+" "+str(cap_value)]
    cap_count+=1
    line=line+["R"+str(res_count)+" "+"n"+str(node)+" "+"n"+str(node+1)+" "+str(res_value)]
    res_count+=1
    node+=1
    line=line+["R"+str(res_count)+" "+"n"+str(node)+" "+str(side_b)+" "+str(res_conv_b)]
    res_count+=1
    res_count+=1
    node+=1
    return [line, cap_count, res_count, node]

class netlist_class:
        def __init__(self,filename, lines):
            self.name = filename
            self.lines=lines
        def __repr__(self):
            return(self.name)
        def __str__(self):
            return(self.name)
        def write_file(self):
            """Writes the lines to the netlist file"""
            file = open(self.name,"w")
            for line in self.lines:
                file.write(line+"\n")
            file.close()
        def run_sim(self):
            # Running the simulation
            # This generates a raw file with the same name as the given file
            run_command =".\ltspice\scad3.exe -b -Run {}".format(self.name)
            subprocess.call(run_command)
        def convert_raw(self, voltage_tocolect):
            extract_output=".\ltspice\ltsputil -xo1 {}.raw {}.txt %16.9e \";\" \"DATA\" time V({})".format(self.name.split(".net")[0], self.name.split(".net")[0],voltage_tocolect)
            #print extract_output
            devnull = open(os.devnull, 'w') #supress output
            subprocess.call(extract_output, stdout=devnull)

def main():
    time_array = []
    # Step 1
    # Fetch data and generate netlist lines
    start_t = time()
    room_id_simulate=3
    [lines, res_count, volt_count, cap_count, zone_node_dict, zone_id_sim]=building_model([room_id_simulate])
    time_array.append(time()-start_t)

    #Step 2
    # write the file
    start_t = time()
    filename="trial.net"
    netlist = netlist_class(filename, lines)
    netlist.write_file()
    time_array.append(time()-start_t)

    # Step 3
    # simulate the file
    start_t = time()
    netlist.run_sim()
    time_array.append(time()-start_t)

    # Step 4
    # convert file from raw format to text
    start_t = time()
    netlist.convert_raw("{}".format(zone_node_dict[zone_id_sim]))
    time_array.append(time()-start_t)

    # Step 5
    #clean all other files
    clean_aux_files()
    print "FINISHED"
    print "-------TIMINGS-------"
    print "Time to fetch data from server & build the model: {0:.1f}ms".format(time_array[0]*1000)
    print "-----------------------------------"
    print "Time to write netlist: {0:.1f}ms".format(time_array[1]*1000)
    print "-----------------------------------"
    print "Total LTSPICE sim: {0:.1f}ms".format(time_array[2]*1000)
    print "-----------------------------------"
    import re
    for line in open(filename[:-4]+'.log'):
        if re.findall(r'(\d+\.\d+)(?= seconds.)',line):
            coretime = float(re.findall(r'(\d+\.\d+)(?= seconds.)',line)[0])
            print "Core LTSPICE sim: {0:.1f}ms".format(coretime*1000)
            print "-----------------------------------"
            break
    print "Binary to ASCII convertion: {0:.1f}ms".format(time_array[3]*1000)
    print "-----------------------------------"
    total = 0
    for t in time_array:
        total += t
    print "Total time: {0:.1f}ms".format(total*1000)

    # Step 5 Sanity check
    print "sanity check"
    print "Resistor count: {}".format(res_count-1)
    print "Voltage sources count: {}".format(volt_count-1)
    print "cap_count: {}".format(cap_count-1)



def clean_aux_files():
    import os
    files = os.listdir('./')
    for file in files:
        if file.endswith(".tmp") or file.endswith(".raw"):
            os.remove(os.path.join('./',file))


if __name__ == "__main__":
    main()