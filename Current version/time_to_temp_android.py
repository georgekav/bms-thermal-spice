__author__ = 'bansal'
from building_model import *
from datetime import date, timedelta, datetime, time
from time import time
import re


def time_to_temperature(user_room_location,set_temp, filename="time2tem_and"):
    time_array = []
    # Step 0: Retrieve user data
    set_point = set_temp
    room_id_simulate = user_room_location
    # Step 1
    # Generate netlist lines
    start_t = time()
    [sim_start_point,sim_end_point] = convert_date_time()
    [lines, res_count, volt_count, cap_count, zone_node_dict, zone_id_simulate]=building_model([room_id_simulate], sim_start_point, sim_end_point)
    time_array.append(time()-start_t)

    #Step 2
    # write the file
    start_t = time()
    netlist = netlist_class(filename, lines)
    length=len(netlist.lines)
    netlist.lines.insert(length-3,"I1 0 n1 PULSE(0 177.8 10 0 0 86100 86100 1)")  # this has to change. right now i am putting a heater source in time to temperature
    netlist.write_file()
    time_array.append(time()-start_t)

    # Step 3
    # simulate the file
    start_t =  time()
    netlist.run_sim()
    time_array.append(time()-start_t)

    # Step 4
    # convert the file
    start_t =  time()
    netlist.convert_raw("{}".format(zone_node_dict[zone_id_simulate]))
    time_array.append(time()-start_t)

    # Step 5
    # check when the temperature is reached
    start_t =  time()
    check= temperature_check("{}.txt".format(netlist.name.split(".net")[0]), set_point)
    time_array.append(time()-start_t)
    if check:
        return(check[0])
    else:
        return False

    # step 4
    #clean all other files


def temperature_check(temperature_filename, set_point):
        """Read the file's lines into a list, removing the endline characters, and placing it in the lines attribute.
            Finds parameter statements and voltage statements and places them in parameters and voltage_parameters attributes correspondingly. """
        file = open(temperature_filename,"r")
        lines = file.readlines()
        for line in lines:
            #remove trailing "\r\n" characters.
            line = line.rstrip()
            print line
            #check for parameters and voltage parameters
            temp_check = temperature_read(line)
            if temp_check != False:
                if ((float(temp_check[1]) >=set_point-0.1) and  (float(temp_check[1]) <=set_point+0.1)):
                    return ([float(temp_check[0]), float(temp_check[1])])
        file.close()
        return False


def temperature_read(line):
    """finds the parameter and value for a given line and parameter"""
    regex_string = "\S+ *; *\S+" ## Regex string to identify pattern corresponding to .param statements
    temp_regex = re.compile(regex_string,re.IGNORECASE)
    temp_find =temp_regex.match(line) ## findall(line) matches all the instances of the regex_string pattern in line
    if temp_find:
        [time, temp] = temp_find.group().split(";")
        return([time, temp])
    else:
        return False

def extract_set_point(user_data):
    set_point = user_data[0]["fields"]["set_value"]
    return set_point

def convert_date_time():
    current_date = datetime.today()
    sim_start_point = current_date + timedelta(days=-1)
    print sim_start_point
    sim_start_point = '{:%Y%m%d%H%M%S}'.format(sim_start_point)
    sim_end_point = current_date + timedelta(days=-1,hours=12)
    sim_end_point = '{:%Y%m%d%H%M%S}'.format(sim_end_point)
    return  sim_start_point,sim_end_point

if __name__ == "__main__":
    main()
    print time_to_temperature(3,24)