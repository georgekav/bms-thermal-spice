__author__ = 'bansal'
from netlist_agent import *
from model_extractor import *


def run_main():
    netlist_fn = "test_12.net"
    #step 1
    original_netlist = fetch_netlist_file(netlist_fn)

    #step 2
    change_params = extract_model("building_data.txt")

    #step 3
    new_netlist = generate_new_netlist(original_netlist, change_params)

    #step 4
    # Simulate the new netlist.
    new_netlist.run_sim()
    new_netlist.convert_raw("vr")

    #step 5
    #clean all other files
    clean_aux_files()
    print "FINISHED"



def fetch_netlist_file(net_filename):
    # Read netlist file into the netlist object
    original_filename = net_filename
    original_netlist = netlist_class(original_filename)
    return original_netlist


def extract_model(build_filename):
    # extract coefficients from building_data file and create a dictionary of RC
    a=building_data_class(build_filename)
    change_params=element_dictionary(a.coefficients)
    return change_params


def generate_new_netlist(original_netlist, change_params):
    # Generate a new netlist from the new parameters
    new_netlist = original_netlist.change_parameters(change_params)
    #Write the new netlist to file
    new_netlist.write_file()
    return new_netlist

def clean_aux_files():
    import os
    files = os.listdir('./')
    for file in files:
        if file.endswith(".tmp") or file.endswith(".raw"):
            os.remove(os.path.join('./',file))

if __name__ == "__main__":
    #if the file is run directly by this script run the main
    run_main()