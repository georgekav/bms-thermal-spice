__author__ = 'bansal'
__author__ = 'bansal'

from remote_data_fetcher import *
import re
import os
import sys
import copy
import subprocess
import math
from time import time
class netlist_class:

        def __init__(self):
            self.name=""; # filename
            self.node=1; # counter to keep track of nodes
            self.cap_count=1; # counter to keep track of capacitances
            self.res_count=1; # counter to keep track of resitances
            self.volt_count=1;
            self.lines=["This is a work of EPFL"];
            self.zone_lines=[]; # this is used to store the lines for the zones which either act as voltage sources or capacitors
            self.zone_node_dict={};
            self.building_data= get_static_data(1);
            self.zone_list=self.building_data["Zonelist"];
            self.d_a=1.2041;
            self.cp_a=1012;
        def __repr__(self):
            return(self.name)
        def __str__(self):
            return(self.name)
        def building_model(self):
            bd_element_list= self.building_data["ZoneBorderlist"]
            for zone in self.zone_list:
                self.zone_node_dict[zone["zoneID"]]="n"+str(self.node);
                self.node+=1;
            # write resistances and capacitances for building elements like walls and windows
            [self.lines, self.zone_node_dict, self.node, self.cap_count, self.res_count]= write_building_element(bd_element_list, self.lines,self.zone_node_dict, self.node, self.cap_count, self.res_count)
            return self.lines;

        def zone_components(self, zone_ids_simulate_list):
             for zone in self.zone_list:
                if zone["zoneID"] in zone_ids_simulate_list:
                    [self.zone_lines, self.zone_node_dict, self.node, self.cap_count]=write_zone_capacitors(zone, self.zone_lines, self.zone_node_dict, self.node, self.cap_count, self.d_a,self.cp_a)
                else:
                    [self.zone_lines, self.zone_node_dict, self.node, self.volt_count]=write_voltage_sources(zone, self.zone_lines, self.zone_node_dict, self.node, self.volt_count)
             return self.zone_lines

        def write_file(self,lines):
            """Writes the lines to the netlist file"""
            file = open(self.name,"w")
            for line in lines:
                file.write(line+"\n")
            file.close()
        def run_sim(self):
            # Running the simulation
            # This generates a raw file with the same name as the given file
            run_command ="..\ltspice\scad3.exe -b -Run {}".format(self.name)
            subprocess.call(run_command)
        def convert_raw(self, voltage_tocolect):
            extract_output="..\ltspice\ltsputil -xo1 {}.raw {}.txt %16.9e \";\" \"DATA\" time V({})".format(self.name.split(".net")[0], self.name.split(".net")[0],voltage_tocolect)
            #print extract_output
            devnull = open(os.devnull, 'w') #supress output
            subprocess.call(extract_output, stdout=devnull)



def zone_components(zone_ids_simulate_list,zone_node_dict, zone_list, lines,cap_count, volt_count):
     for zone in zone_list:
        if zone["zoneID"] in zone_ids_simulate_list:
            [lines, zone_node_dict, node, cap_count]=write_zone_capacitors(zone, lines, zone_node_dict, node, cap_count, d_a,cp_a)
        else:
            [lines, zone_node_dict, node, volt_count]=write_voltage_sources(zone, lines, zone_node_dict, node, volt_count)

def write_zone_capacitors(zone, lines, zone_node_dict, node, cap_count, d_a,cp_a):
    cap_value=d_a*cp_a*zone["volume"];
    current_line=["C"+str(cap_count)+" "+str(0)+" "+"n"+str(node)+" "+str(cap_value)];
    cap_count+=1;
    node+=1;
    lines=lines+current_line;
    return[lines, zone_node_dict,node, cap_count]

def write_voltage_sources(zone, lines, zone_node_dict, node, volt_count):
    volt_value=str(get_dynamic_data(roomid=zone["roomID"], f_time=20141116000000, t_time=20141116235500, res=120))
    current_line = ["V"+str(volt_count)+" "+"n"+str(node)+" "+str(0)+" "+"PWL"+str(volt_value)]
    volt_count+=1;
    node+=1;
    lines=lines+current_line;
    return [lines, zone_node_dict, node, volt_count]


def write_building_element(bd_element_list, lines,zone_node_dict, node, cap_count, res_count):
    for bd_element in bd_element_list:
            side_a = zone_node_dict[bd_element["l_zoneID"]];
            side_b = zone_node_dict[bd_element["r_zoneID"]];
            area = bd_element["surface"];
            conv_coeff_a = bd_element["zb_attr"]["conjA"];
            conv_coeff_b = bd_element["zb_attr"]["conjB"];
            cp=[];
            d=[];
            rho=[];
            t=[];
            if bd_element["window_attr"]!=None: # check if the wall has a window
                win_area = bd_element["window_surface"];
                u_value = bd_element["window_attr"]["uvalue"];
                area=area-win_area; # subtracting the area of window from wall area
                if area >=0:
                    res_win=1/(win_area*u_value);
                    lines=lines+["R"+str(res_count)+" "+str(side_a)+" "+str(side_b)+" "+str(res_win)];
                    node+=1
                    res_count+=1;
                else:
                    raise ValueError("window area cannot be larger than wall area")
            i=0;
            for material in  bd_element["zb_attr"]["matlist"]:
                cp = cp+ [bd_element["zb_attr"]["matlist"][i]["matterial"]["cp"]];
                d = d + [bd_element["zb_attr"]["matlist"][i]["matterial"]["D"]]
                rho = rho+ [bd_element["zb_attr"]["matlist"][i]["matterial"]["p"]]
                t=t+[bd_element["zb_attr"]["matlist"][i]["thickness"]]
                i=i+1
                print cp
            [res_value,cap_value,res_conv_a, res_conv_b]=compute_resistance(cp,d,rho,t, area, conv_coeff_a, conv_coeff_b);
#            writing the wall resistance and capacitances
            [current_line, cap_count, res_count, node]= compose_line(res_count,cap_count, res_value,cap_value, node, side_a, side_b,res_conv_a,res_conv_b);
            lines=lines+current_line;
    print lines;
    lines.append(".tran 0 86100");
    lines.append(".backanno");
    lines.append(".end");
    return [lines, zone_node_dict, node, cap_count, res_count]



def compute_resistance(cp,d,rho,t, area, conv_a, conv_b):
    total_t=sum(t);
    davg = sum ([d[i]*t[i] for i in range(len(d))])/total_t;
    cpavg = sum ([cp[i]*t[i] for i in range(len(d))])/total_t;
    print cpavg
    print cp
    print t
    rhoavg = sum ([rho[i]*t[i] for i in range(len(d))])/total_t;
    res = (rhoavg*total_t)/(2*area);
    cap = davg*total_t*area*cpavg;
    res_conv_a=1/(area*conv_a);
    res_conv_b=1/(area*conv_b);
    return [res,cap, res_conv_a,res_conv_b]


def compose_line(res_count, cap_count, res_value, cap_value, node, side_a, side_b, res_conv_a, res_conv_b):
    line=[];
    line=line+["R"+str(res_count)+" "+str(side_a)+" "+"n"+str(node+1)+" "+str(res_conv_a)];
    res_count+=1;
    node+=1;
    line=line+["R"+str(res_count)+" "+"n"+str(node)+" "+"n"+str(node+1)+" "+str(res_value)];
    res_count+=1;
    node+=1;
    line=line+["C"+str(cap_count)+" "+str(0)+" "+"n"+str(node)+" "+str(cap_value)]
    cap_count+=1;
    line=line+["R"+str(res_count)+" "+"n"+str(node)+" "+"n"+str(node+1)+" "+str(res_value)];
    res_count+=1;
    node+=1;
    line=line+["R"+str(res_count)+" "+"n"+str(node)+" "+str(side_b)+" "+str(res_conv_b)];
    res_count+=1;
    node+=1;
    return [line, cap_count, res_count, node]


def main():
      time_array = []
    # Step 1
    # Generate netlist lines
    zone_id_simulate=6
    start_t = time()
    building_object=netlist_class()
    static_lines=building_object.building_model()
    dynamic_lines=building_object.zone_components([zone_id_simulate])
    time_array.append(time()-start_t)

    #Step 2
    # write the file
    start_t = time()
    filename="test_14.net";
    lines= static_lines + dynamic_lines
    building_object.name=filename
    building_object.write_file(lines);
    time_array.append(time()-start_t)
    # Step 3
    # simulate the file
    start_t = time()
    building_object.run_sim();
    time_array.append(time()-start_t)
    # Step 4
    # simulate the file
    start_t = time()
    building_object.convert_raw("{}".format(building_object.zone_node_dict[zone_id_simulate]));
    time_array.append(time()-start_t)
    print "{}".format(building_object.zone_node_dict[zone_id_simulate])
    #step 5
    #clean all other files, not timed
    clean_aux_files()
    print "FINISHED"
    print "-------TIMINGS-------"
    print "Reading old netlist: {0:.1f}ms".format(time_array[0]*1000)
    print "-----------------------------------"
    print "Extracting model: {0:.1f}ms".format(time_array[1]*1000)
    print "-----------------------------------"
    print "Generating new netlist: {0:.1f}ms".format(time_array[2]*1000)
    print "-----------------------------------"
    print "Total LTSPICE sim: {0:.1f}ms".format(time_array[3]*1000)
    print "-----------------------------------"
    import re
    for line in open(netlist_fn[:-4]+'_new.log'):
        if re.findall(r'(\d+\.\d+)(?= seconds.)',line):
            coretime = float(re.findall(r'(\d+\.\d+)(?= seconds.)',line)[0])
            print "Core LTSPICE sim: {0:.1f}ms".format(coretime*1000)
            print "-----------------------------------"
            break
    print "Binary to ASCII convertion: {0:.1f}ms".format(time_array[4]*1000)
    print "-----------------------------------"
    total = 0
    for t in time_array:
        total += t
    print "Total time: {0:.1f}ms".format(total*1000)

    clean_aux_files()
    print "FINISHED"

    print building_object.res_count;
    print building_object.volt_count;
    print building_object.cap_count;



def clean_aux_files():
    import os
    files = os.listdir('./')
    for file in files:
        if file.endswith(".tmp") or file.endswith(".raw"):
            os.remove(os.path.join('./',file))


if __name__ == "__main__":
    main()